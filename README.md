Mapa dos Serviços Livres
=====

1. [Sobre o Mapa](#sobre-o-mapa)

2. [Como Contribuir](#como-contribuir)

## Sobre o Mapa
Esse projeto como objetivo criar uma mapa e formulário para coletar e permitir a visualizaçõa de dados de empresas que trabalham com implementação de sistemas baseados em software livre e recursos educacionais abertos para a área da educação. Isso inclui instalação e manutenção, software as service e formação nas instituições e redes de ensino. A empresa não precisa exclusivamente trabalhar com software livre, mas deve ter expertise e uma carta de serviços baseados em software livre.

Foi desenvolvido em **REACT** com **MongoDB**.

Deploy atual está em: [http://mapadosoftware.educacaoaberta.org/](http://mapadosoftware.educacaoaberta.org).

## Como Contribuir
Por favor leia nosso [Código de Conduta](./conduta.md) antes de contribuir.
[Nosso guia de instalação](SETUP.md) para quem gostaria de contribuir está [aqui](SETUP.md).

1. **Encontre um Issue**
Busque um [issue](https://gitlab.com/EducacaoAberta/mapadosoftware/-/issues?label_name%5B%5D=To+Do) que gostaria de completar. Aquelas marcadas com `todo` são as de maior prioridade

2. **Faça um fork do repositório**
Um fork é uma cópia que permite você explorar e experimentar sem afetar o projeto original. Aprenda mais [aqui](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)

3. **Crie um branch**
 Pode usar qualquer nome mas o mais fácil para reconhecer tem o formato `XXXX-descricao-do-trabalho`. Tutorial [aqui](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch)

4. **Manda braza!** 
Siga as instruções de [como configurar seu computador](SETUP.md). Faça perguntas no `issue` original para esclarecer quaisquer dúvidas. Ganhe pontos extras se incluir testes de regressão ;) Faça commits descritivos.

5. **Faça push do seu branch e faça um merge request**
Tutorial [aqui](https://docs.gitlab.com/ee/user/project/merge_requests/) sobre o push e merge request.

### Code Review & Merge
Depois de fazer um merge request, um dos nossos contribuidores principais fará uma revisão do trabalho. Geralmente haverão algumas sugestões ou perguntas. Depois de receber um sinal verde, você poderá fazer o merge.

### Por Favor Foque no Issue
Tente não incluir trabalhos adicionais no mesmo branch. Se você notar outras coisas, favor criar um issue novo ou um comentário (`TODO`, `FIXME`, `NOTE`).

### Merge Request com Trabalho Incompleto
As vezes é necessário fazer o merge request antes de completar o trabalho para que outros possam oferecer feedback. Não há problma! Mas por favor anote isso com `in-progress` assim todos saberão do status.

### Torne-se um Contribuidor Principal
Se você se tornar um contribuidor frequente, e participa das conversas, você poderá se tornar um contribuidor principal.