const prod = {
  url: {
	  API_URL: 'https://msa.aberta.org.br/api',
  }
};

const dev = {
  url: {
    API_URL: 'http://localhost:9000/api'
  }
};

export const config = process.env.NODE_ENV === 'development' ? dev : prod;

export const DATABASE = process.env.NODE_ENV === 'development' ? process.env.STAGING_DATABASE : process.env.PRODUCTION_DATABASE;
