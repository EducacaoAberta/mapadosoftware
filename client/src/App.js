import React, { useState, useEffect } from "react";
import api from "./api";
import Sidebar from "./components/sidebar/Sidebar";
import { Map } from "./components/map/Map";
import "./App.css";

function App() {
  const [position, setPosition] = useState({ lng: null, lat: null });
  const [dados, setDados] = useState([]);
  const [dadosFiltrados, setDadosFiltrados] = useState([]);
  const [flyToCoords, setFlyToCoords] = useState({ lng: null, lat: null });

  function insertNewPoint(point) {
    if (point.midiaSocial[0].length === 0 && point.midiaSocial[0].plataforma === "")
      point.midiaSocial.pop(); //remover plataforma vazia

    setDados([...dados, point]);
  }

  useEffect(() => {
    (async () => {
      await api.getAllPoints().then((points) => {
        setDados(points.data.data);
        setDadosFiltrados(points.data.data);
      });
    })();
  }, []);

  return (
    <div className='App'>
      <Sidebar
        position={position}
        dados={dados}
        getDadosFiltrados={setDadosFiltrados}
        getFlyToCoords={setFlyToCoords}
        getNewPoint={insertNewPoint}
      />
      <Map
        getPosition={setPosition}
        dadosFiltrados={dadosFiltrados}
        flyToCoords={flyToCoords}
      />
    </div>
  );
}

export default App;
