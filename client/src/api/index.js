import axios from 'axios'
import { config } from '../Constants'

axios.defaults.baseURL = config.url.API_URL

export const insertPoint = payload => axios.post(`/point`, payload)
export const getAllPoints = () => axios.get(`/points`)
export const updatePointById = (id, payload) => axios.put(`/point/${id}`, payload)
export const deletePointById = id => axios.delete(`/point/${id}`)
export const getPointById = id => axios.get(`/point/${id}`)

const apis = {
  insertPoint,
  getAllPoints,
  updatePointById,
  deletePointById,
  getPointById,
}

export default apis