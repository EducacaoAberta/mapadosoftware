import React, { Fragment } from "react";
import { PointForm } from "../cadastro/Form";
import { Sobre } from "./Sobre";
import { TermosDeUso } from "./TermosDeUso";
import PropTypes from "prop-types";
import { Tabs, Tab } from "react-materialize";
import logo from "../../images/Logo_MSA.png";

import "./Sidebar.css";
import { Prestadores } from "../prestadores/Prestadores";
import { PoliticaDePrivacidade } from "./PoliticaDePrivacidade";

export default function Sidebar(props) {
  const { position, dados, getDadosFiltrados, getFlyToCoords, getNewPoint } = props;

  return (
    <div id='sidebar'>
      <div className='sidebar-main'>
        <div className='sidebar-header'>
          <a href="/">
            <img className='logomsa' src={logo} alt='logo_msa' />
          </a>

          <div id='geocoder'></div>
        </div>
        <div className='sidebar-nav'>
          <Tabs className='z-depth-1 tabs-fixed-width'>
            <Tab
              active
              options={{
                duration: 300,
                onShow: null,
                responsiveThreshold: Infinity,
                swipeable: false,
              }}
              title='Cadastro'
            >
              {pointForm(position, getNewPoint)}
            </Tab>
            <Tab
              options={{
                duration: 300,
                onShow: null,
                responsiveThreshold: Infinity,
                swipeable: false,
              }}
              title='Prestadores'
            >
              <Prestadores
                dados={dados}
                getDadosFiltrados={getDadosFiltrados}
                getFlyToCoords={getFlyToCoords}
              />
            </Tab>
          </Tabs>
        </div>
      </div>

      <footer className='page-footer'>
        <div className='footer-links'>
          <Sobre />
          <TermosDeUso />
          <PoliticaDePrivacidade />
        </div>
        <div className='footer-copyright'>
          <div className='container'>
          <p>
            Os conteúdos desse site estão sob uma licença{" "}
            <a href='https://creativecommons.org/licenses/by-sa/3.0/igo/.'>
            CC BY-SA 3.0 IGO.
            </a>
          </p>
          </div>
        </div>
      </footer>
    </div>
  );
}

function pointForm(position, getNewPoint) {
  if (position.lat && position.lng) {
    return <PointForm 
      position={position} 
      getNewPoint={getNewPoint}
    />;
  } else {
    return (
      <Fragment>
        <p>Seja bem vindo ao Mapa de Serviços Abertos!</p>

        <p>
          Este mapa tem como objetivo coletar e disponibilizar um catálogo de
          prestadores que oferecem serviços com base em software livre e/ou
          recursos educacionais abertos, voltados para a área da educação. Com
          isso, buscamos facilitar o contato entre prestadores e gestores,
          promovendo o livre e o aberto na educação!
        </p>

        <p>
          O prestador não precisa exclusivamente trabalhar com software livre ou 
          recursos educacionais abertos para a educação, mas deve ter uma carta de 
          serviços em software livre e/ou recursos educacionais abertos.
        </p>

        <p>
          Se você deseja se <strong>cadastrar</strong> você tem duas opções:
          procure seu endereço na caixa acima, ou identifique um ponto no mapa e
          preencha seu cadastro que ficará visível imediatamente.
        </p>

        <p>
          Se você deseja <strong>encontrar prestadores de serviço</strong>,
          clique na aba PRESTADORES e navegue pelo mapa, ou faça sua busca.
        </p>
      </Fragment>
    );
  }
}

Sidebar.propTypes = {
  position: PropTypes.exact({
    lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  dados: PropTypes.array,
  getDadosFiltrados: PropTypes.func,
  getFlyToCoords: PropTypes.func,
  getNewPoint: PropTypes.func,
};