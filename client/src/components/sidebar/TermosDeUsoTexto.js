import React from 'react';

export const TermosDeUsoTexto = () => {
    return (
        <div className="termos">
            <p>A presente versão destes Termos de Uso foi atualizada pela última vez em <strong>28 de agosto de 2020</strong>.</p>

            <p>Ao utilizar o “Mapa de serviços abertos” (MSA), o usuário aceita todas as condições aqui estabelecidas.</p>

            <p>A aceitação destes “Termos de Uso” é indispensável à utilização do MSA. Os usuários deverão ler, compreender e aceitar todas as condições 
                neles estabelecidas. Dessa forma, torna-se claro que a utilização do MSA implica a aceitação completa dos presentes Termos de Uso. Caso haja 
                dúvidas sobre os Termos, entre em contato pelo <i>e-mail</i>: contato@aberta.org.br, com o assunto “MSA”.</p>

            <p><strong>1. Participação do usuário</strong></p>
            <p>A participação pode ocorrer pelo envio de cadastro realizado no <i>site</i>.</p>
            <p>O envio de conteúdo está condicionado ao preenchimento dos campos indicados no formulário, em que apenas alguns são obrigatórios. O conteúdo 
                será automaticamente disponibilizado no <i>site</i>.</p>
            <p>Periodicamente, os conteúdos cadastrados serão revisados por membros do projeto, para verificação de erros e remoção daqueles que não estejam 
                alinhados à proposta do MSA. Caso seja necessária a averiguação de dados, o MSA poderá entrar em contato com o usuário pelo <i>e-mail</i> de cadastro.</p>
            <p>O MSA não se responsabiliza por qualquer dano, prejuízo ou perda no equipamento do usuário causado por falhas no sistema, no servidor ou na 
                internet decorrentes de condutas de terceiros, de vírus por acesso, da utilização da internet ou da transferência de dados.</p>
            <p>Todos os conteúdos publicados são considerados públicos e visíveis por qualquer pessoa, sem nenhum tipo de confidencialidade.</p>
            <p>Não serão aceitos conteúdos que não estejam alinhados à proposta do MSA, ou que contenham vocabulário ofensivo ou desrespeitoso a terceiros, 
                incluindo ofensas, calúnias, injúrias e difamações. Também não serão permitidas indicações de materiais que invadam a privacidade de terceiros, 
                que sejam ameaçadoras, preconceituosas ou racistas.</p>
            <p>Caso o usuário entenda que alguma postagem, material, comentário ou outro conteúdo disponibilizado no MSA viole algum de seus direitos, a legislação 
                aplicável ou as cláusulas destes Termos de Uso, deverá notificar tal fato pelor <i>e-mail</i>: contato@aberta.org.br, com o assunto “MSA”.</p>

            <p><strong>1.1 É permitido ao usuário:</strong></p>
            <p>Criar um cadastro de um serviço que seja alinhado à proposta do MSA.</p>

            <p><strong>1.2 É vedado ao usuário:</strong></p>
            <p>Transmitir, exibir, enviar ou, de qualquer outra forma, disponibilizar conteúdo que contenha material pornográfico e/ou atividades ilegais 
                relativas a menores de 18 (dezoito) anos (consoante o Estatuto da Criança e do Adolescente – ECA), que invada a privacidade de terceiros, 
                que tenha cunho comercial, viole direitos humanos ou seja ilegal, ofensivo, ameaçador, que incite a violência, seja vulgar, preconceituoso, 
                ou racista, como descrito nos artigos 138 a 140 do Código Penal Brasileiro, ou de qualquer forma contrário às cláusulas destes Termos de Uso;</p>
            <p>Assumir a identidade de outra pessoa, física ou jurídica; forjar cabeçalhos, ou de qualquer outra forma, manipular identificadores a fim de 
                disfarçar a origem de qualquer material contido na plataforma, com o sentido de desmoralizar, desprestigiar ou se fazer passar pelo MSA;</p>
            <p>Disponibilizar conteúdo em desconformidade com a legislação de direito autoral, incluindo conteúdo que viole marca registrada, patente, segredo 
                de negócio, direito autoral ou qualquer outro direito de terceiro; disponibilizar conteúdo com qualquer tipo de propaganda, material promocional, 
                {" "}<i>spam</i> (mensagens não solicitadas), correntes ou esquemas de pirâmide;</p>
            <p>Disponibilizar conteúdo que contenha vírus ou qualquer outro código, arquivo ou programa de computador, com o propósito de interromper, 
                destruir ou limitar a funcionalidade de qualquer <i>software</i>, <i>hardware</i> ou equipamento de telecomunicações;</p>
            <p>Interferir ou interromper os serviços, as redes ou os servidores conectados à plataforma, de modo a dificultar a utilização e o aproveitamento dos 
                serviços por outros usuários, bem como obter ou tentar obter acesso não autorizado à plataforma;</p>
            <p>Pedir votos, mencionar número de candidato ou expressar qualquer outra manifestação que se caracterize como propaganda política ou que viole 
                as normas eleitorais.</p>

            <p><strong>2. Licença de uso</strong></p>
            <p>O MSA é uma realização da Iniciativa Educação Aberta. Tem seu código disponível no <a href="https://gitlab.com/EducacaoAberta/mapadosoftware/">Gitlab</a>, 
                com licença GPLv3. Os conteúdos do <i>site</i> e os dados de prestadores estão disponíveis com a licença{" "}
                <a href="https://creativecommons.org/licenses/by-sa/3.0/igo/">Creative Commons Atribuição-Compartilha Igual IGO 3.0</a>.</p>
            <p>O MSA poderá disponibilizar endereços eletrônicos de outros <i>sites</i> externos, por meio de <i>links</i>, o que não significa que estes sejam de 
                sua propriedade ou por ele operados. A presença de <i>links</i> para outros <i>sites</i> não implica relação de sociedade ou de supervisão do MSA 
                com esses <i>sites</i> e seus conteúdos.</p>
            
            <p><strong>3. Limitação de responsabilidade</strong></p>
            <p>Em nenhuma situação o MSA é responsável por quaisquer danos, prejuízos ou outros efeitos, diretos ou indiretos, relacionados ao uso, por parte de seus 
                usuários, leitores ou de qualquer outra pessoa, deste <i>site</i>, de seu conteúdo ou de qualquer outro <i>site</i> mencionado.</p>
            
            <p><strong>4. Inexistência de vínculo</strong></p>
            <p>A adesão a estes Termos de Uso pelo usuário não gera nenhum contrato, mandato, franquia ou vínculo de tipo trabalhista, societário, de parceria ou 
                associativo entre o MSA e o usuário.</p>

            <p><strong>5. Como reportar violações</strong></p>
            <p>Se algum conteúdo violar algum direito do usuário, de terceiros ou a legislação aplicável, o usuário poderá entrar em contato com a administração do 
                MSA e enviar um <i>e-mail</i> para contato@aberta.org.br, com o assunto “MSA”, informando o problema de forma específica e detalhada.</p>
            <p>De posse de tais informações, o MSA poderá analisar e resolver a questão tão breve quanto possível. Caso as informações estejam incompletas, ou com 
                detalhamento insuficiente, o MSA poderá entrar em contato com o usuário para solicitar a complementação, o que possivelmente atrasará a providência desejada.</p>

            <p><strong>6. Modificações e foro</strong></p>
            <p>O MSA poderá alterar estes Termos de Uso a qualquer tempo, mediante declaração pública na própria página de Termos de Uso, visando ao seu aprimoramento 
                e à sua melhoria. Os novos Termos de Uso entrarão em vigor a partir de sua publicação na plataforma.</p>
            <p>Para dirimir dúvidas ou litígios referentes à interpretação e ao cumprimento destes Termos de Uso, fica eleito o foro da comarca em que se encontra 
                a sede do editor do <i>site</i>.</p>
        </div>
    );
}