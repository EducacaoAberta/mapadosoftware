import React from 'react';
import { Modal, Button } from 'react-materialize';
import { PoliticaDePrivacidadeTexto } from './PoliticaDePrivacidadeTexto'

import './TermosDeUsoPrivacidade.css';

export const PoliticaDePrivacidade = () => {
    return (
        <div>
            <Modal
                actions={[
                    <Button flat modal="close" node="button" waves="light">Fechar</Button>
                    ]}
                bottomSheet={false}
                fixedFooter={false}
                header="Política de Privacidade"
                id="Modal-0"
                open={false}
                options={{
                    dismissible: true,
                    endingTop: '5%',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    opacity: 0.5,
                    outDuration: 250,
                    preventScrolling: true,
                    startingTop: '5%'
                    }}
                trigger={<Button flat waves="light" node="button">Política de Privacidade</Button>}
                >
                <PoliticaDePrivacidadeTexto />
            </Modal>
        </div>
    )
}