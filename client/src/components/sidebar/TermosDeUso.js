import React from 'react';
import { Modal, Button } from 'react-materialize';
import { TermosDeUsoTexto } from './TermosDeUsoTexto';

import './TermosDeUsoPrivacidade.css';

export const TermosDeUso = () => {
    return (
        <div>
            <Modal
                actions={[
                    <Button flat modal="close" node="button" waves="light">Fechar</Button>
                    ]}
                bottomSheet={false}
                fixedFooter={false}
                header="Termos de Uso"
                id="Modal-0"
                open={false}
                options={{
                    dismissible: true,
                    endingTop: '5%',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    opacity: 0.5,
                    outDuration: 250,
                    preventScrolling: true,
                    startingTop: '5%'
                    }}
                trigger={<Button flat waves="light" node="button">Termos de uso</Button>}
                >
                <TermosDeUsoTexto />
            </Modal>
        </div>
    )
}