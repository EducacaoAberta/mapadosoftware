import React from 'react';
import { Modal, Button } from 'react-materialize';

import './Sobre.css';

import logoUnesco from '../../images/logo_UNESCO_Brasil_conjunto_Catedra_cinza.png';
import logoIEA from '../../images/Logo_IEA.png';

export const Sobre = () => {
    return (
        <div>
            <Modal
                actions={[
                    <Button flat modal="close" node="button" waves="light">Fechar</Button>
                    ]}
                bottomSheet={false}
                fixedFooter={false}
                header="Sobre"
                id="Modal-0"
                open={false}
                options={{
                    dismissible: true,
                    endingTop: '5%',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    opacity: 0.5,
                    outDuration: 250,
                    preventScrolling: true,
                    startingTop: '5%'
                    }}
                trigger={<Button flat waves="light" node="button">Sobre</Button>}
            >
                <p>
                O “Mapa de serviços abertos” (MSA) é um espaço permanente de inserção e disponibilização de informações sobre prestadores de serviço que trabalham com a 
                lógica e a ética do software livre e dos recursos educacionais abertos (REA), dando visibilidade ao reconhecido protagonismo brasileiro nessas duas áreas.
                </p>

                <p>
                O MSA tem como objetivo ajudar governos, instituições e gestores, que têm um papel importantíssimo na construção do ecossistema digital de professores e 
                estudantes, a conhecerem alternativas aos serviços “gratuitos” oferecidos por grandes empresas. Outro objetivo consiste em mostrar a existência e os contatos 
                de prestadores de serviço que podem ajudar a implementar soluções alternativas e seguras, além de oferecer esses serviços de forma rápida e confiável. 
                </p>

                <p>
                O prestador não precisa trabalhar exclusivamente com software livre ou com REA, mas deve ter conhecimento e um portfólio de serviços nessa área, voltados 
                para a educação.
                </p>

                <p>
                O cadastro é feito diretamente por qualquer prestador de serviço. Para cada novo cadastro, nós visitamos o(s) site(s) e o(s) link(s) indicado(s), verificamos 
                se aparentam ser prestadoras reais e se apresentam oferta de produtos e soluções em software livre e REA. 
                </p>

                <p>
                Somos um site informativo e não oferecemos qualquer garantia quanto aos dados e à qualidade dos serviços prestados. Portanto, sugerimos que você averígue a 
                qualidade dos serviços e solicite um portfólio de trabalhos prévios, contatos para recomendação, ou mesmo pesquise em sites como o Reclame Aqui e o do Instituto 
                Brasileiro de Defesa do Consumidor (IDEC), entre outros que prestam serviços de verificação.
                </p>                

                <h2>O Projeto</h2>
                <p>
                Este site é uma realização da <a href="https://aberta.org.br">Iniciativa Educação Aberta</a> e resultado de cooperação com a UNESCO, como parte do contrato 
                ED00283/2020, que tem como objetivo prover ferramentas de acesso livre e gratuito para experimentação e uso, por parte de atores da educação. 
                A coordenação do projeto é de <a href="https://amiel.info">Tel Amiel</a>, sua programação foi feita por <a href="https://kalimar.github.io/">Kalimar Maia</a>
                {" "} e <a href="https://rafa-ce.github.io/">Rafael Contessotto</a>, e seu  código está disponível no <a href="https://gitlab.com/EducacaoAberta/mapadosoftware">
                Gitlab</a>, com licença GPL3. Todo seu conteúdo está disponível com a licença <a href="https://creativecommons.org/licenses/by-sa/3.0/igo/">CC-BY-SA 3.0 IGO</a>.
                </p>

                <p>
                A UNESCO não endossa nenhum produto, serviço, marca ou empresa. Veja a <a href="https://en.unesco.org/covid19/legalnotice">nota jurídica</a> (em inglês). 
                A UNESCO também não garante que as informações, os documentos e os materiais contidos em seu site sejam completos e corretos, e que não serão responsáveis 
                por quaisquer danos sofridos como resultado de seu uso. Veja aqui a <a href="https://en.unesco.org/this-site/access-to-information-policy">Política de 
                Acesso</a> à informação da UNESCO. Veja aqui a <a href="https://en.unesco.org/this-site/our-online-privacy-policy"> Política de Privacidade</a> online da UNESCO. 
                E veja aqui os <a href="https://en.unesco.org/logopatronage"> Termos de Uso</a> do nome e da logo da UNESCO.
                </p>

                <h2>Erros? Dúvidas? Comentários?</h2>
                <p>
                Caso você encontre algum erro nas informações ou tenha alguma dúvida ou sugestão, entre em contato por meio dos canais disponíveis na {" "}
                <a href="https://aberta.org.br">Iniciativa Educação Aberta</a>.
                </p>

                <div className="logos">
                    <a  className="logounesco" href="//www.educacaoaberta.org" target='_blank' rel='noopener noreferrer'>
                        <img src={logoUnesco} alt="logo_unesco" />
                    </a>
                    <a className="logoiea" href="//www.aberta.org.br" target='_blank' rel='noopener noreferrer'>
                        <img src={logoIEA} alt="logo_iea" />
                    </a>
                </div>
            </Modal>
        </div>
    )
}