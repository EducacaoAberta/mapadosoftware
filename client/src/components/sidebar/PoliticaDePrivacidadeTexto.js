import React from 'react';

export const PoliticaDePrivacidadeTexto = () => {
    return (
        <div className="termos">
            <p>A presente versão desta Política de Privacidade foi atualizada pela última vez em <strong>28 de agosto de 2020</strong>.</p>

            <p><strong>Resumo</strong></p>
            <p>Se você <strong>navegar</strong> neste site, ele não coletará nenhum dado pessoal de forma automática. O site não faz uso de{" "} 
                <em>cookies</em>. Caso você escolha <strong>cadastrar</strong> sua prestadora de serviço, os dados que você inserir estarão automaticamente 
                disponíveis com a licença <a href="https://creativecommons.org/licenses/by-sa/3.0/igo/">Creative Commons Atribuição-Compartilha Igual 
                IGO 3.0</a>. Esses dados poderão ser utilizados pelos administradores do MSA ou por qualquer outro visitante do site, nos termos dessa 
                licença, ou seja, podem ser utilizados contanto que seja atribuído ao “Mapa de Serviços Abertos” (MSA), utilizando a mesma licença e 
                incluindo um link para o site do MSA e um link para a licença. Nós poderemos revisar os dados para corrigir erros ou remover 
                cadastros que não sejam adequados à proposta do MSA. Você poderá ou não ser notificado. A qualquer momento, caso não concorde 
                com quaisquer dados publicados, você pode pedir para que seu cadastro seja removido.</p>
            
            <p><strong>1. Informações gerais</strong></p>
            <p>A presente Política de Privacidade contém informações a respeito do modo como tratamos, total ou parcialmente, de forma automatizada 
                ou não, os dados pessoais dos usuários que acessam nosso <em>site</em>. Seu objetivo é esclarecer os interessados acerca dos tipos de 
                dados que são coletados, dos motivos da coleta e da forma como o usuário poderá atualizar, gerenciar ou excluir estas informações.</p>
            <p>Esta Política de Privacidade foi elaborada em conformidade com a Lei Federal n. 12.965 de 23 de abril de 2014 (Marco Civil da Internet), 
                com a Lei Federal n. 13.709, de 14 de agosto de 2018 (Lei de Proteção de Dados Pessoais) e com o Regulamento UE n. 2016/679 de 27 de 
                abril de 2016 (Regulamento Geral Europeu de Proteção de Dados Pessoais - RGDP).</p>
            <p>Esta Política de Privacidade poderá ser atualizada em decorrência de eventual atualização normativa, razão pela qual se convida o usuário 
                a consultar periodicamente esta seção.</p>
            
            <p><strong>2. Direitos do usuário</strong></p>
            <p>O <em>site</em> se compromete a cumprir as normas previstas no RGDP em respeito aos seguintes princípios:</p>
            <ul>
                <li>
                    <p>Os dados pessoais do usuário serão processados de forma lícita, leal e transparente (licitude, lealdade e transparência);</p>
                </li>
                <li>
                    <p>Os dados pessoais do usuário serão coletados apenas para finalidades determinadas, explícitas e legítimas; posteriormente, não 
                        poderão ser tratados de uma forma incompatível com essas finalidades (limitação das finalidades);</p>                    
                </li>
                <li>
                    <p>Os dados pessoais do usuário serão coletados de forma adequada, pertinente e limitada às necessidades do objetivo para os quais 
                        eles são processados (minimização dos dados);</p>                    
                </li>
                <li>
                    <p>Os dados pessoais do usuário serão exatos e atualizados sempre que necessário, de maneira que os dados inexatos sejam apagados 
                        ou retificados quando possível (exatidão);</p>                    
                </li>
                <li>
                    <p>Os dados pessoais do usuário serão conservados de uma forma que permita a identificação dos titulares dos dados apenas durante o 
                        período necessário às finalidades para as quais são tratados (limitação da conservação);</p>                    
                </li>
                <li>
                    <p>Os dados pessoais do usuário serão tratados de forma segura, protegidos do tratamento não autorizado ou ilícito e contra sua perda, 
                        destruição ou danificação acidental, adotando-se as medidas técnicas ou organizativas adequadas (integridade e confidencialidade).</p>                    
                </li>
            </ul>
            
            <p>O usuário do <em>site</em> possui os seguintes direitos, conferidos pela Lei de Proteção de Dados Pessoais e pelo RGDP:</p>
            <ul>
                <li>
                    <p><em>Direito de confirmação e acesso</em> – é o direito do usuário de obter do <em>site</em> a confirmação de que os dados pessoais que 
                    lhe digam respeito são ou não objeto de tratamento e, se for esse o caso, o direito de acessar os seus dados pessoais;</p>
                </li>
                <li>
                    <p><em>Direito de retificação</em> – é o direito do usuário de obter do <em>site</em>, sem demora injustificada, a retificação dos dados 
                    pessoais inexatos que lhe digam respeito;</p>
                </li>
                <li>
                    <p><em>Direito à eliminação dos dados (direito ao esquecimento)</em> – é o direito do usuário de ter seus dados apagados do <em>site</em>;</p>
                </li>
                <li>
                    <p><em>Direito à limitação do tratamento dos dados</em> – é o direito do usuário de limitar o tratamento de seus dados pessoais, podendo 
                    obtê-lo quando contesta a exatidão dos dados, quando o tratamento for ilícito, quando o <em>site</em> não precisar mais dos dados para as 
                    finalidades propostas, quando tiver se oposto ao tratamento dos dados e em caso de tratamento de dados desnecessários;</p>
                </li>
                <li>
                    <p><em>Direito de oposição</em> – é o direito do usuário de, a qualquer momento, se opor por motivos relacionados com sua situação particular, 
                    ao tratamento dos dados pessoais que lhe digam respeito; o usuário também pode se opor ao uso de seus dados pessoais para a definição de 
                    perfil de <em>marketing (profiling)</em>;</p>
                </li>
                <li>
                    <p><em>Direito de portabilidade dos dados</em> – é o direito do usuário de receber os dados pessoais que lhe digam respeito e que tenha fornecido 
                    ao <em>site</em>, em um formato estruturado, de uso corrente e de leitura automática, além do direito de transmitir esses dados para outro <em>site</em>;</p>
                </li>
                <li>
                    <p><em>Direito de não ser submetido a decisões automatizadas</em> – é o direito do usuário de não se sujeitar a nenhuma decisão tomada de forma exclusiva 
                    com base no tratamento automatizado, incluindo a definição de perfis (<em>profiling</em>), que produza efeitos em sua esfera jurídica ou que o afete de 
                    forma similar e significativa.</p>
                </li>
            </ul>
            
            <p>O usuário poderá exercer os seus direitos por meio de comunicação escrita enviada ao <em>site</em>, especificando:</p>
            <ul>
                <li>
                    <p>nome completo ou razão social;</p>
                </li>
                <li>
                    <p>número do Cadastro de Pessoas Físicas (CPF) ou do Cadastro Nacional de Pessoa Jurídica (CNPJ), ambos da Receita Federal do Brasil;</p>
                </li>
                <li>
                    <p>endereço de <em>e-mail</em> do usuário e, se for o caso, do seu representante;</p>
                </li>
                <li>
                    <p>direito que deseja exercer junto ao <em>site</em>;</p>
                </li>
                <li>
                    <p>data do pedido;</p>
                </li>
                <li>
                    <p>assinatura do usuário; e</p>
                </li>
                <li>
                    <p>todos os documentos que possam demonstrar ou justificar o exercício do seu direito.</p>
                </li>
            </ul>
            <p>O pedido deverá ser enviado ao seguinte <em>e-mail</em>: contato@aberta.org.br. O usuário será informado em caso de retificação ou eliminação dos seus dados.</p>
            
            <p><strong>3. Dever de não fornecer dados de terceiros</strong></p>
            <p>Durante a utilização do <em>site</em>, a fim de resguardar e proteger os direitos de terceiros, o usuário deverá fornecer somente seus dados pessoais, 
            e não os de terceiros.</p>
            
            <p><strong>4. Informações coletadas</strong></p>
            <p>A coleta de dados dos usuários se dará em conformidade com o disposto nesta Política de Privacidade e dependerá do consentimento do próprio usuário, sendo 
                este dispensável somente nas hipóteses previstas no artigo 11, inciso II, da Lei de Proteção de Dados Pessoais.</p>
            
            <p><strong>4.1. Tipos de dados coletados</strong></p>
            <p><em><strong>4.1.1. Dados de identificação do usuário para a realização de cadastro</strong></em></p>
            <p>Não demandamos nem solicitamos cadastro para visualizar o <em>site</em> e seus dados. Caso você queira, voluntariamente, registrar a sua prestadora de 
                serviços, os dados que você inserir serão armazenados e disponibilizados imediatamente no <em>site</em>, com a licença Creative Commons Atribuição-Compartilha 
                Igual IGO 3.0. Esses dados são armazenados nos servidores da Iniciativa Educação Aberta.</p>
            <p><em><strong>4.1.2 Dados coletados automaticamente</strong></em></p>
            <p>Para funcionamento, nossos servidores coletam, de forma automática, alguns dados da sua conexão (em arquivos chamados logs).</p>
            <p><em><strong>4.1.3. Dados sensíveis</strong></em></p>
            <p>Não serão coletados dados sensíveis dos usuários, assim entendidos aqueles definidos nos artigos 9º e 10 do RGDP e nos artigos 11 e seguintes da Lei de 
                Proteção de Dados Pessoais.</p>
            
            <p><strong>4.2. Fundamento jurídico para o tratamento dos dados pessoais</strong></p>
            <p>Ao utilizar os serviços do site, o usuário consente e concorda com a presente Política de Privacidade.</p>
            <p>O usuário tem o direito de retirar seu consentimento a qualquer momento, o que não compromete a licitude do tratamento de seus dados pessoais antes da 
                retirada. A retirada do consentimento poderá ser feita pelo <em>e-mail</em>: contato@aberta.org.br, com o assunto “MSA”.</p>
            <p>O tratamento de dados pessoais sem o consentimento do usuário somente será realizado em razão de interesse legítimo ou nas hipóteses previstas em lei.</p>
                        
            <p><strong>4.3. Finalidades do tratamento dos dados pessoais</strong></p>
            <p>Os dados pessoais do usuário coletados pelo <em>site</em> têm por finalidade facilitar, agilizar e cumprir os compromissos estabelecidos com o usuário e fazer 
                cumprir as solicitações realizadas por meio do preenchimento de formulários.</p>
            <p>Os dados coletados pelo formulário de cadastro também poderão ser utilizados para a produção de relatórios e estudos, bem como para outros fins, inclusive 
                comerciais por terceiros, conforme os termos da licença CC-BY-SA 3.0 IGO.</p>
            <p>Os dados poderão ser utilizados para fornecer subsídios ao <em>site</em> para a melhora da qualidade e do funcionamento de seus serviços.</p>
            <p>O tratamento de dados pessoais para finalidades não previstas nesta Política de Privacidade somente ocorrerá mediante comunicação prévia ao usuário, sendo 
                que, em qualquer caso, permanecerão aplicáveis os direitos e as obrigações aqui previstos.</p>
            
            <p><strong>4.4. Prazo de conservação dos dados pessoais</strong></p>
            <p>Os dados pessoais do usuário serão conservados por um período não superior ao exigido para cumprir os objetivos em razão dos quais eles são processados. 
                O período de conservação dos dados é definido de acordo com o seguinte critério:</p>
            <ul>
                <li>
                    <p>acesso permanente enquanto existir a plataforma.</p>
                </li>
            </ul>
                        
            <p><strong>4.5. Destinatários e transferência e tratamento dos dados pessoais</strong></p>
            <p>Os dados inseridos pelos usuários no cadastro de prestadores de serviço são considerados abertos, conforme os termos da licença CC BY-SA 3.0 IGO. 
                Sendo assim, os dados poderão ser acessados, baixados e utilizados por terceiros, nos termos dessa licença. Dados de acesso (<em>logs</em>) não serão 
                tratados nem disponibilizados.</p>
            
            <p><strong>5. Do responsável pela proteção e tratamento dos dados</strong></p>
            <p>Neste <em>site</em>, a Iniciativa Educação Aberta é o responsável pelo tratamento dos dados pessoais coletados. Ela poderá ser contactada pelo{" "} 
                <em>e-mail</em>: contato@aberta.org.br, com o assunto “MSA”.</p>
            
            <p><strong>6. Segurança no tratamento dos dados pessoais do usuário</strong></p>
            <p>O <em>site</em> não coleta dados pessoais automaticamente. Os dados inseridos por você no sistema podem conter dados pessoais.</p>
            <p>O <em>site</em> se compromete a aplicar as medidas técnicas e organizativas aptas a proteger os dados pessoais de acessos não autorizados e de situações 
                de destruição, perda, alteração, comunicação ou difusão de tais dados.</p>
            <p>Para a garantia da segurança, serão adotadas soluções que levem em consideração: as técnicas adequadas, os custos de aplicação, a natureza, o âmbito, 
                o contexto e as finalidades do tratamento, bem como os riscos para os direitos e para as liberdades do usuário.</p>
            <p>No entanto, o <em>site</em> se exime de responsabilidade por culpa exclusiva de terceiros, como no caso de ataque de <em>crackers</em>; ou por culpa 
                exclusiva do usuário, como no caso em que ele mesmo transfere seus dados a terceiros. Além disso, o <em>site</em> se compromete a comunicar ao usuário 
                em prazo adequado, caso ocorra algum tipo de violação da segurança de seus dados pessoais que possa lhe ocasionar um alto risco para seus direitos e 
                liberdades pessoais.</p>
            <p>A violação de dados pessoais é uma violação de segurança que provoque, de modo acidental ou ilícito, a destruição, a perda, a alteração, a divulgação 
                ou o acesso não autorizados a dados pessoais transmitidos, conservados ou sujeitos a qualquer outro tipo de tratamento.</p>
            <p>Por fim, o <em>site</em> se compromete a tratar os dados pessoais do usuário com confidencialidade, dentro dos limites legais.</p>
            
            <p><strong>7. <em>Cookies</em> do <em>site</em></strong></p>
            <p>Os <em>cookies</em> utilizados pelo <em>site</em> não contêm informações que permitem a identificação do usuário. O <em>site</em> não utiliza{" "}
            <em>plugins</em> de redes sociais.</p>
            
            <p><strong>8. Das alterações</strong></p>
            <p>O editor se reserva o direito de modificar, a qualquer momento e sem qualquer aviso prévio, as presentes normas do <em>site</em>, especialmente para 
                adaptá-las às evoluções do “Mapa de serviços abertos” (MSA), seja pela disponibilização de novas funcionalidades, seja pela supressão ou pela modificação 
                daquelas já existentes.</p>
            <p>Dessa forma, convidamos o usuário a consultar periodicamente esta página para verificar as atualizações.</p>
            <p>Ao utilizar o serviço após eventuais modificações, o usuário demonstra sua concordância com as novas normas.</p>
            
            <p><strong>9. Do direito aplicável e do foro</strong></p>
            <p>Para a solução das controvérsias decorrentes do presente instrumento, será aplicado integralmente o direito brasileiro.</p>
            <p>Os eventuais litígios deverão ser apresentados no foro da comarca em que se encontra a sede do editor do <em>site</em>.</p>
        </div>
    );
}