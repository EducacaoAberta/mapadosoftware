import React, { useState, useEffect, Fragment } from 'react';
import { Button, Checkbox } from 'react-materialize';
import PropTypes from "prop-types";

import './SelectArea.css';

const areas = [
    "Biblioteca/Gestão de recursos educacionais",
    "Conteúdo",
    "Currículo/gestão curricular",
    "E-learning/LMS",
    "Ferramentas de comunicação",
    "Hardware e redes",
    "Plataformas de capacitação/Formação",
    "Tecnologia assistiva"
]

export const SelectArea = (props) => {
    const { getFiltroArea } = props;

    const [showFiltroArea, setShowFiltroArea] = useState(false);
    const [areasSelecionadas, setAreasSelecionadas] = useState([]);

    useEffect(() => {
        getFiltroArea(areasSelecionadas);
    })
        
    function clickLimparFiltro() {
        setAreasSelecionadas([]);
    }
    
    return (
        <div 
            className="select">
            <Button small className="filtrar-area" onClick={() => setShowFiltroArea(!showFiltroArea)}>
                <span className="select_value">
                    Filtros
                </span>
            </Button>
          
          <div
          className={"select_list " + (!showFiltroArea && 'hide')}>
            {
            areas.map((area, index) =>{
                return (
                        <Fragment key={index}>
                            <Checkbox 
                                filledIn
                                id={area} 
                                label={area} 
                                value={area} 
                                checked={
                                    areasSelecionadas?.filter(e => e === area).length > 0
                                } 
                                onChange={(e) => {
                                    if(e.target.checked) {
                                        setAreasSelecionadas([...areasSelecionadas, area]);
                                    }
                                    else {
                                        setAreasSelecionadas(areasSelecionadas?.filter(e => e !== area));
                                    }
                                }}
                            />
                        </Fragment>)
                })
            }
            <div className="btn-select-area">
                <Button className="select-area blue" node="button" waves="light" onClick={() => clickLimparFiltro()}>Limpar filtros</Button>
            </div>
          </div>
        </div>  
    )
}

SelectArea.propTypes = {
    getFiltroArea: PropTypes.func
};