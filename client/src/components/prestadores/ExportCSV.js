import React from 'react';
import { Button } from 'react-materialize';
import PropTypes from 'prop-types';

export const ExportCSV = (props) => {
    const { dadosExport } = props;

    function camposCSV(campo) {
        return [
            "nomeDaEmpresa", "cnpj", "resumoDaEmpresa", "siteURL", "nomeParaContato", 
            "emailParaContato", "telefoneParaContato", "raioDeAtuacao", "midiaSocial", "expertise"
        ].includes(campo);

    }

    function convertArrayOfObjectsToCSV() {
        const items = dadosExport;
        const replacer = (key, value) => value === null ? '' : value;
        const header = Object.keys(items[0]).filter(camposCSV);
        let csv = items.map(row => {
            return header.map(fieldName => {
                if (fieldName === "midiaSocial") {
                    return row[fieldName].map(midia => `{ ${midia.plataforma}: ${midia.nomeDoUsuario} }`).join(', ')
                }
                else if (fieldName === "expertise") {
                    return row[fieldName].map(expertise => 
                        `{ ${expertise.areaDeAtuacao} : [ ${expertise.servicoOferecido.map(servico => servico.servicoOferecidoTipo + " : " + servico.nomeDoSoftware )} ]}`)
                        .join(', ')
                }
                else {
                    return JSON.stringify(row[fieldName], replacer)
                }
            }).join(';')
        })
        csv.unshift(header.join(';'))
        csv = csv.join('\r\n')

        return csv;
    }

    function downloadCSV() {
        const link = document.createElement('a');
        let csv = convertArrayOfObjectsToCSV();
        if (csv == null) 
            return;
      
        const filename = 'prestadores.csv';
      
        if (!csv.match(/^data:text\/csv/i)) {
          csv = `data:text/csv;charset=utf-8,${csv}`;
        }
      
        link.setAttribute('href', encodeURI(csv));
        link.setAttribute('download', filename);
        link.click();
    }  

    return(
        <Button 
            disabled = { dadosExport.length === 0 }
            className="download-csv" 
            small
            onClick={() => downloadCSV()}
            tooltip="Baixar tabela de prestadores (.csv)">
                <i className="fas fa-file-download fa-2x"></i>
        </Button>
    )
}

ExportCSV.propTypes = {
    dadosExport: PropTypes.array
}