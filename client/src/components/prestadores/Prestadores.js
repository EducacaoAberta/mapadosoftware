import React, { useEffect, Fragment, useState } from 'react';
import DataTable from 'react-data-table-component';
import PropTypes from "prop-types";
import { Card, Icon } from 'react-materialize';
import { SelectArea } from './SelectArea';
import { ExportCSV } from './ExportCSV';
import './Prestadores.css';

const columns = [
  {
    selector: "nomeDaEmpresa",
    width: "90%",
    cell: (row) => <Prestador row={row} />,
  },
];

export const Prestadores = (props) => {
  const { dados, getDadosFiltrados, getFlyToCoords } = props;

  const [filtroTexto, setFiltroTexto] = useState('');
  const [filtroAreaSelecionada, setFiltroArea] = useState([]);

  const [dadosArea, setDadosArea] = useState([]);
  const [dadosTabela, setDadosTabela] = useState([]);

  const [rowExpandedId, setRowExpandedId] = useState('');
    
  function filtrarTexto() {
    if (filtroTexto === "" && (filtroAreaSelecionada.length === 0 || filtroAreaSelecionada.length === 8))
        return dados;

    return dadosArea.filter(
      (item) =>
        item.nomeDaEmpresa.toLowerCase().includes(filtroTexto.toLowerCase()) ||
        item.resumoDaEmpresa
          .toLowerCase()
          .includes(filtroTexto.toLocaleLowerCase()) ||
        item.expertise.filter((e) => {
          return JSON.stringify(e.servicoOferecido)
            .toLowerCase()
            .includes(filtroTexto.toLocaleLowerCase());
        }).length > 0
    );
  }

  function filtrarArea() {
    if (filtroAreaSelecionada.length === 0) return dados;

    return dados.filter(
      (item) =>
        filtroAreaSelecionada.filter(function (area) {
          return JSON.stringify(item.expertise).includes(area);
        }).length > 0
    );
  }

  function expandClick(expanded, row) {
    if (expanded) {
      getFlyToCoords(row.position)
      setRowExpandedId(row._id)
    }
    else {
      getFlyToCoords(null)
      setRowExpandedId('')
    }
  }

  useEffect(() => {
    setDadosTabela(filtrarTexto());
  }, [dadosArea, filtroTexto]);

  useEffect(() => {
    getDadosFiltrados(dadosTabela);
  }, [dadosTabela]);

  useEffect(() => {
    setDadosArea(filtrarArea());
  }, [dados, filtroAreaSelecionada]);

  const subHeaderComponentMemo = React.useMemo(() => {
    return (
        <div className="filtros">
            <FilterAreaComponent getFiltroArea={setFiltroArea} />
            <FilterTextComponent onFiltroTexto={e => 
              setFiltroTexto(e.target.value)} 
              filtroTexto={filtroTexto} />
            <ExportCSV dadosExport={dadosTabela} />
        </div>
    );

}, [filtroTexto, dadosTabela]);

  const paginationOptions = {
    noRowsPerPage: true,
    rangeSeparatorText: "de",
    selectAllRowsItem: true,
    selectAllRowsItemText: "Todos",
  };  

  return (
    <div className='dataTableWrapper'>
      <DataTable
        className='tabela'
        columns={columns}
        data={dadosTabela}
        pagination
        expandableRows
        expandableRowsComponent={
          <PrestadorInfo></PrestadorInfo>
        }
        onRowExpandToggled={(expanded, row) => { expandClick(expanded, row)}}
        expandableRowExpanded={ (row) =>{ return row._id === rowExpandedId}}
        noHeader={true}
        noTableHead={true}
        subHeader
        subHeaderComponent={subHeaderComponentMemo}
        paginationComponentOptions={paginationOptions}
        noDataComponent={<EmptyComponent />}
      />
    </div>
  );
};

const Prestador = ({ row }) => (
  <div>
    <div className='prestador-nome'>
      <h6>{row.nomeDaEmpresa}</h6>
      <a
        className='linkSite'
        href={row.siteURL}
        target='_blank'
        rel='noopener noreferrer'
      >
        <i className="fas fa-external-link-alt fa-sm"></i>
      </a>
    </div>
    <div className='resumoWrapper'>
      <div
        className='resumoEmpresa'
        style={{
          color: "grey",
          overflow: "hidden",
          whiteSpace: "wrap",
          textOverflow: "ellipses",
        }}
      >
        <p>{row.resumoDaEmpresa}</p>
      </div>
    </div>
  </div>
);

const PrestadorInfo = (props) => {
  return (
    <Card
      className='white'
      closeIcon={<Icon>close</Icon>}
      revealIcon={<Icon>more_vert</Icon>}
    >
      <ul className='lista-area'>
        <p>
          <strong>Área de atuação</strong>
        </p>
        <Card className='servicos'>
          {props.data.expertise.map((elem, index) => {
            return (
              <li key={index}>
                <strong>{elem.areaDeAtuacao}</strong>
                <ul>
                  {elem.servicoOferecido.map((servico, indexServico) => {
                    return (
                      <div key={indexServico}>
                        <li>
                          {servico.nomeDoSoftware === "" ? (
                            <p>{servico.servicoOferecidoTipo}</p>
                          ) : (
                            <p>
                              {servico.servicoOferecidoTipo}:{" "}
                              {servico.nomeDoSoftware}
                            </p>
                          )}
                        </li>
                      </div>
                    );
                  })}
                </ul>
              </li>
            );
          })}
        </Card>
      </ul>

      <ul>
        <p>
          <strong>Informações</strong>
        </p>
        <Card className='informacoes'>
          <li>
            <p>
              <strong>Nome: </strong>
              {props.data.nomeParaContato}
            </p>
          </li>
          <li>
            <p>
              <strong>E-mail: </strong>
              {props.data.emailParaContato}
            </p>
          </li>
          {props.data.telefoneParaContato !== "" && (
            <li>
              <p>
                <strong>Telefone: </strong>
                {props.data.telefoneParaContato}
              </p>
            </li>
          )}
          <li>
            <p>
              <strong>Raio de atuação: </strong>
              {props.data.raioDeAtuacao}
            </p>
            {props.data.cnpj !== "" && (
              <p>
                <strong>CNPJ: </strong> {props.data.cnpj}
              </p>
            )}
          </li>
          {props.data.midiaSocial.map((elem, index) => {
            return (
              <Fragment key={index}>
                {elem.plataforma !== "" && (
                  <li>
                    <strong>{elem.plataforma}:</strong> {elem.nomeDoUsuario}
                  </li>
                )}
              </Fragment>
            );
          })}
        </Card>
      </ul>
    </Card>
  );
};

const FilterTextComponent = ({ filtroTexto, onFiltroTexto }) => (
  <Fragment>
    <input
      id='filtrar'
      type='text'
      placeholder='Procurar por nome/serviço/software'
      value={filtroTexto}
      onChange={onFiltroTexto}
    />
  </Fragment>
);

const FilterAreaComponent = (props) => (
  <SelectArea getFiltroArea={props.getFiltroArea} />
);

const EmptyComponent = () => (
  <div className='tabela-vazia'>
    <h2>Não foram encontrados prestadores</h2>
  </div>
);

Prestadores.propTypes = {
  dados: PropTypes.array,
  getDadosFiltrados: PropTypes.func,
  getFlyToCoords: PropTypes.func,
};
