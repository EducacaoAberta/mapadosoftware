import React, { useState, useRef, useEffect } from "react";
import mapboxgl from "mapbox-gl";
import turf from "turf";
import PropTypes from "prop-types";
import MapboxGeocoder from "@mapbox/mapbox-gl-geocoder";
import MapboxLanguage from "@mapbox/mapbox-gl-language";

const generateInnerHTML = (properties) => {
  let HTMLString = "<div class='popupHTML'>";
  HTMLString += properties.nomeDaEmpresa
    ? `<h5>${properties.nomeDaEmpresa}</h5>`
    : "";
  HTMLString += properties.resumoDaEmpresa
    ? `<p class='popup-sectionEntry'>${properties.resumoDaEmpresa}</p>`
    : "";
  HTMLString += properties.siteURL
    ? `<p class='popup-sectionHeader'><a href='${properties.siteURL}' target='_blank' >${properties.siteURL}</a></p>`
    : "";
  const parsedExpertise = JSON.parse(properties.expertise);
  if (parsedExpertise.length > 0) {
    HTMLString += "<div class='popup-section'>";
    HTMLString += "<p class='popup-sectionHeader'>Serviços Oferecidos</p>";
    parsedExpertise.forEach((expertise) => {
      HTMLString += `<p class='popup-sectionEntry'>${expertise.areaDeAtuacao}`;
    });
    HTMLString += "</div>";
  }
  HTMLString += properties.raioDeAtuacao
    ? `<p class='popup-sectionHeader'>Raio de Atuação</p><p class='popup-sectionEntry'>${properties.raioDeAtuacao}</p>`
    : "";
  const parsedMidiaSocial = JSON.parse(properties.midiaSocial);
  if (parsedMidiaSocial.length > 0) {
    HTMLString += "<div class='popup-section'>";
    HTMLString += "<p class='popup-sectionHeader'>Mídia Social</p>";
    parsedMidiaSocial.forEach((midiaSocial) => {
      HTMLString += `<p class='popup-sectionEntry'>${midiaSocial.plataforma} - ${midiaSocial.nomeDoUsuario}</p>`;
    });
  }
  HTMLString += properties.cnpj
    ? `<p class="popup-sectionHeader">CNPJ</p><p class="popup-sectionEntry">${properties.cnpj}</p>`
    : "";
  HTMLString += "</div>";
  return HTMLString;
};

const getPointCollection = (map, dados) => {
  let pointCollection;

  const pointArray = dados.map((point) => {
    const {
      cnpj,
      emailParaContato,
      expertise,
      midiaSocial,
      nomeDaEmpresa,
      NomeParaContato,
      raioDeAtuacao,
      resumoDaEmpresa,
      siteURL,
      telefoneParaContato,
      position,
    } = point;
    return turf.point([position.lng, position.lat], {
      cnpj,
      emailParaContato,
      expertise,
      midiaSocial,
      nomeDaEmpresa,
      NomeParaContato,
      raioDeAtuacao,
      resumoDaEmpresa,
      siteURL,
      telefoneParaContato,
    });
  });
  pointCollection = turf.featureCollection(pointArray);

  map.getSource("pointCollection").setData(pointCollection);
};

export const Map = (props) => {
  const { getPosition, dadosFiltrados, flyToCoords } = props;

  const [map, setMap] = useState(null);
  const mapContainer = useRef(null);

  useEffect(() => {
    mapboxgl.accessToken =
      "pk.eyJ1IjoiaW5pY2lhdGl2YSIsImEiOiJja2EzNmdmaGwwbDVkM2d0YXhpMmIwdXM2In0.md1yyMUsj-NRLQs29Ze-6w";
    const initializeMap = ({ setMap, mapContainer }) => {
      const map = new mapboxgl.Map({
        container: mapContainer.current,
        style: "mapbox://styles/iniciativa/cka372ipk04sp1is6qxybu5i6",
        center: [-55.16, -16.28],
        zoom: 3.7,
      });

      const marker = new mapboxgl.Marker({ draggable: true, color: "#3658D6" });

      map.on("load", () => {
        setMap(map);
      });

      map.on("load", () => {
        map.addSource("pointCollection", {
          type: "geojson",
          data: turf.featureCollection([]),
          cluster: true,
          clusterMaxZoom: 14,
          clusterRadius: 50,
        });

        map.addLayer({
          id: "clusters",
          type: "circle",
          source: "pointCollection",
          filter: ["has", "point_count"],
          paint: {
            // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
            // with three steps to implement three types of circles:
            // * Blue, 20px circles when point count is less than 100
            // * Yellow, 30px circles when point count is between 100 and 750
            // * Pink, 40px circles when point count is greater than or equal to 750
            "circle-color": [
              "step",
              ["get", "point_count"],
              "#38a0b6",
              100,
              "#fbb03b",
              750,
              "#e55e5e",
            ],
            "circle-radius": [
              "step",
              ["get", "point_count"],
              20,
              100,
              30,
              750,
              40,
            ],
          },
        });

        map.addLayer({
          id: "cluster-count",
          type: "symbol",
          source: "pointCollection",
          filter: ["has", "point_count"],
          paint: {
            "text-color": "white",
          },
          layout: {
            "text-field": "{point_count_abbreviated}",
            "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
            "text-size": 12,
          },
        });

        map.addLayer({
          id: "unclustered-point",
          source: "pointCollection",
          type: "circle",
          filter: ["!", ["has", "point_count"]],
          paint: {
            "circle-color": "rgb(207, 76, 76)",
            "circle-radius": {
              base: 1.75,
              stops: [
                [12, 6],
                [22, 180],
              ],
            },
            "circle-stroke-width": 1,
            "circle-stroke-color": "#fff",
          },
        });
      });

      map.on("click", "clusters", (e) => {
        const features = map.queryRenderedFeatures(e.point, {
          layers: ["clusters"],
        });
        const clusterId = features[0].properties.cluster_id;

        map
          .getSource("pointCollection")
          .getClusterExpansionZoom(clusterId, (err, zoom) => {
            if (err) return;

            map.easeTo({
              center: features[0].geometry.coordinates,
              zoom: zoom,
            });
          });
      });

      map.on("click", "unclustered-point", (e) => {
        const coordinates = e.features[0].geometry.coordinates.slice();
        const properties = e.features[0].properties;
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }
        const innerHTML = generateInnerHTML(properties);
        new mapboxgl.Popup()
          .setLngLat(coordinates)
          .setHTML(innerHTML)
          .addTo(map);
      });

      map.on("mouseenter", "unclustered-point", () => {
        map.getCanvas().style.cursor = "pointer";
      });

      map.on("mouseleave", "unclustered-point", () => {
        map.getCanvas().style.cursor = "";
      });

      map.on("click", (e) => {
        if (
          map.queryRenderedFeatures(e.point, {
            layers: ["clusters", "unclustered-point"],
          }).length
        )
          return;

        const { lng, lat } = e.lngLat;
        //this.setState({ lng: lng.toFixed(4), lat: lat.toFixed(4) });
        // const position = turf.point([lng.toFixed(4), lat.toFixed(4)]);

        if (marker.getLngLat()) {
          marker.setLngLat([lng, lat]);
        } else {
          marker.setLngLat([lng, lat]).addTo(map);
        }

        getPosition({ lng: lng.toFixed(4), lat: lat.toFixed(4) });
      });

      const geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        language: "pt-BR",
        mapboxgl: mapboxgl,
        color: "#3658D6",
        marker: false,
      });

      document.getElementById("geocoder").appendChild(geocoder.onAdd(map));

      geocoder.on("result", (ev) => {
        const { geometry } = ev.result;

        const [lng, lat] = geometry.coordinates;

        if (marker.getLngLat()) {
          marker.setLngLat([lng, lat]);
        } else {
          marker.setLngLat([lng, lat]).addTo(map);
        }

        if (lng && lat) {
          getPosition({
            lng: lng.toFixed(4),
            lat: lat.toFixed(4),
          });
        }
      });

      map.addControl(new MapboxLanguage());
      map.addControl(new mapboxgl.NavigationControl());
    };

    if (!map) {
      initializeMap({ setMap, mapContainer });
    }
  }, [map]);

  useEffect(() => {
    if (map) {
      getPointCollection(map, dadosFiltrados);
    }
  }, [map, dadosFiltrados]);

  useEffect(() => {
    if (flyToCoords && map) {
      map.flyTo({ center: [flyToCoords.lng, flyToCoords.lat], zoom: 10 });
    }
  }, [flyToCoords]);

  return <div ref={(el) => (mapContainer.current = el)} id='map'></div>;
};

Map.propTypes = {
  getPosition: PropTypes.func,
  dadosFiltrados: PropTypes.array,
  position: PropTypes.exact({
    lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  })
};
