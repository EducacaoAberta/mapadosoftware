Configuração de Sistema
======
1. **Faça Um Clone** 
[Faça um clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) do repositório (geralmente será um [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) do projeto original)

2. **O projeto tem dois aplicativos** 
Um servidor de API criado com [Express](https://expressjs.com/) e uma interface para o usuário criada com [React](https://reactjs.org/).

3. **Requirimentos**
    * [NODE LTS](https://nodejs.org/en/)
    * [NPM](https://www.npmjs.com/) ou [YARN](https://yarnpkg.com/)
    * MongoDB - Nós usamos uma conta gratuita no [Mongodb.com](https://www.mongodb.com/cloud/atlas/register) mas você pode [instalar localmente também](https://docs.mongodb.com/manual/installation/)

4. **Configure o DB** use como base o `.env.exemplo` (encontrado na pasta `api`) para criar o seu próprio `.env` indicando o DB para desenvolvimento.

5. **Faça instalação dos Node modules**
Cada um dos aplicativos requer instalação. e.g. `yarn --cwd client install` & `yarn --cwd api install` 

6. **Inicie os dois aplicativos** 
Abra uma aba no seu terminal e inicie o client: `yarn --cwd client start`. Abra outra aba para a API: `yarn --cwd api start`.

7. **Endpoints**
O seu aplicativo cliente estará rodando em `localhost:3000`. A API pode ser acessado usando `localhost:9000`. Usamos só dois *endpoints* no momento: `POST` para criar um - `localhost:9000/api/point` e `GET` todos os pontos `localhost:9000/api/points`.

Usando Docker
======
1. **Requirimentos**
    * [Docker](http://docker.com) 
    * [Docker-compose](https://docs.docker.com/compose)

2. **Realize as configurações necessárias**
Crie as configurações de banco de dados como indicado no item 4 da sessão anterior, altere os parametros do docker-compose.yml se achar necessário e indique o usuário, senha e banco de dados à serem criados no MongoDB em mongo-initdb.js

3. **Construa os containers e compile as aplicações**
De dentro do diretório clonado, execute: `docker-compose build`

4. **Suba os containers**
De dentro do diretório clonado, execute: `docker-compose up` para visualiar os logs ou `docker-compose up -d` para rodar como deamon.

5. **Dica: tudo em um**
Alternativamente você pode construir os containers, compilar as aplicações e subir-los com um único comando: `docker-compose up --build` ou `docker-compose up -d --build`
