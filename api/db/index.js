require("dotenv").config();
const mongoose = require("mongoose");

const DATABASE = process.env.NODE_ENV === 'development' ? process.env.STAGING_DATABASE : process.env.PRODUCTION_DATABASE;

mongoose
  .connect(DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .catch((err) => {
    console.error(err);
  });

const db = mongoose.connection
  .on("open", () => {
    console.log("Mongoose connection open");
  })
  .on("error", (err) => {
    console.log(`Connection error: ${err.message}`);
  });

module.exports = db;
