const Point = require('../models/point-model');

createPoint = (req, res) => {
  const body = req.body

  if (!body) {
    return res.status(400).json({
      success: false,
      error: 'You must provide coordinates'
    });
  }

  const point = new Point(body)

  if (!point) {
    return res.status(400).json({ success: false, error: err });
  }

  console.log('saving');
  point
    .save()
    .then(() => {
      console.log('in then');
      return res.status(201).json({
        success: true,
        id: point._id,
        message: 'Point created.'
      });
    })
    .catch(error => {
      console.log('in error');
      return res.status(400).json({
        error,
        message: 'Point not created!'
      });
    });
}

updatePoint = () => null;
// updatePoint = async (req, res) => {
//   const body = req.body;

//   if (!body) {
//     return res.status(400).json({
//       success: false,
//       error: 'You must provide a body to update',
//     })
//   }

//   Point.findOne({ _id: req.params.id }, (err, point) => {
//     if (err) {
//       return res.status(404).json({
//         err,
//         message: 'Point not found!',
//       })
//     }
//     point.position = body.position
//     point.label = body.label
//     point
//       .save()
//       .then(() => {
//         return res.status(200).json({
//           success: true,
//           id: point._id,
//           message: 'Point updated!',
//         })
//       })
//       .catch(error => {
//         return res.status(404).json({
//           error,
//           message: 'point not updated!',
//         });
//       });
//   })
// }

deletePoint = async (req, res) => {
  await Point.findOneAndDelete({ _id: req.params.id }, (err, point) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }

    if (!point) {
      return res
        .status(404)
        .json({ success: false, error: 'Point not found' });
    }

    return res.status(200).json({ success: true, data: point })
  }).catch(err => console.log(err))
}

getPointById = async (req, res) => {
  await Point.findOne({ _id: req.params.id }, (err, point) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }

    return res.status(200).json({ success: true, data: point });
  }).catch(err => console.log(err))
}

getPoints = async (req, res) => {
  await Point.find({}, (err, points) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }
    if (!points.length) {
      return res
        .status(404)
        .json({ success: false, error: 'Points not found' });
    }
    return res.status(200).json({ success: true, data: points });
  }).catch(err => console.log(err))
}

module.exports = {
  createPoint,
  updatePoint,
  deletePoint,
  getPoints,
  getPointById
}