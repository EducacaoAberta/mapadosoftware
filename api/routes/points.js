const express = require('express');

const PointCtrl = require('../controllers/point-ctrl');

const router = express.Router();

router.post('/point', PointCtrl.createPoint);
router.put('/point/:id', PointCtrl.updatePoint);
router.delete('/point/:id', PointCtrl.deletePoint);
router.get('/point/:id', PointCtrl.getPointById);
router.get('/points', PointCtrl.getPoints);

module.exports = router